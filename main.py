import cv2
from utils import CFEVideoConf, image_resize
import glob
import math
import numpy as np

faixa_de_confianca = 0.55 

capitura = cv2.VideoCapture(0)
capitura.set(3,1280)
capitura.set(4,720)
capitura.set(10,70)

classNames= []
classFile = 'coco.names'
with open(classFile,'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')

configPath = 'ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'
weightsPath = 'frozen_inference_graph.pb'

net = cv2.dnn_DetectionModel(weightsPath,configPath)
net.setInputSize(320,320)
net.setInputScale(1.0/ 127.5)
net.setInputMean((127.5, 127.5, 127.5))
net.setInputSwapRB(True)


while True:
    success,img = capitura.read()
    img2 = cv2.cvtColor(img.copy(), cv2.COLOR_BGRA2BGR)
    img3 = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2GRAY)
    img3 = cv2.merge([img3, img3, img3])
    classIds, confs, bbox = net.detect(img3,confThreshold=faixa_de_confianca)
    print(classIds,bbox)

    if len(classIds) != 0:
        for classId, confidence,box in zip(classIds.flatten(),confs.flatten(),bbox):
            cv2.rectangle(img,box,color=(0,255,0),thickness=2)
            cv2.putText(img,classNames[classId-1].upper(),(box[0]+10,box[1]+30),
                        cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)
            cv2.putText(img,str(round(confidence*100,2)),(box[0]+200,box[1]+30),
                        cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)

            cv2.rectangle(img2,box,color=(0,255,0),thickness=2)
            cv2.putText(img2,classNames[classId-1].upper(),(box[0]+10,box[1]+30),
                        cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)
            cv2.putText(img2,str(round(confidence*100,2)),(box[0]+200,box[1]+30),
                        cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)

            cv2.rectangle(img3,box,color=(0,255,0),thickness=2)
            cv2.putText(img3,classNames[classId-1].upper(),(box[0]+10,box[1]+30),
                        cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)
            cv2.putText(img3,str(round(confidence*100,2)),(box[0]+200,box[1]+30),
                        cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)

    cv2.imshow("Output",img)
    cv2.imshow("Red",img2)
    cv2.imshow("Grey",img3)

    cv2.waitKey(1)